<div align="center">
  <img src="https://i.imgur.com/thKzPkw.png">
</div>

This is The Resolution Board, a template created to help you achieve your New Year Resolutions and these are my new year resolutions.

## Resolution 1 📚 ![](https://img.shields.io/badge/progress-8.33%25-red.svg)
This year my my resolution is that I will read at least 12 books.

- [ ] [Hard things about hard things](https://www.amazon.com/Hard-Thing-About-Things-Building/dp/0062273205) - *Ben Horowitz*
- [x] [A Short Guide to a Long Life](https://www.amazon.com/Short-Guide-Long-Life/dp/147673609X) - *David B. Agus M.D.*
- [ ] [Thirty Essentials: Strategy](https://www.amazon.com/Thirty-Essentials-Strategy-strategy-companies-ebook/dp/B079LQVXNB) - *Andrew Laurie*
- [ ] [Small Giants](https://www.amazon.com/Small-Giants-Companies-Instead-10th-Anniversary/dp/014310960X) - *Bo Burlingham*
- [ ] [Tools of titan](https://www.amazon.com/Tools-Titans-Billionaires-World-Class-Performers/dp/1328683788/ref=sr_1_2?ie=UTF8&qid=1546324870&sr=8-2&keywords=tools+of+titans) - *Timothy Ferriss*

## Resolution 2 👨🏻‍💻 ![](https://img.shields.io/badge/progress-33%25-yellow.svg)

This year my resolution is that I will work on 12 open source projects

- [x] [sarthology/ResolutionBoard](https://github.com/sarthology/ResolutionBoard)
- [x] [sarthology/Animatopy](https://github.com/sarthology/Animatopy)
- [x] [sarthology/dev10](https://github.com/sarthology/Dev10)
- [x] [sarthology/devtocli](https://github.com/sarthology/devtocli)
- [ ] [sarthology/nomoogle](https://github.com/sarthology/nomoogle)



## Resolution 3 ✈️ ![](https://img.shields.io/badge/progress-100%25-green.svg)
Taiwan - From **2 Feb to 1 March** Long Trip.

## Resolution 4 💪🏼 ![](https://img.shields.io/badge/progress-17%25-red.svg)
This year I will maintain my body weight to 65-67kg (*current*)
* **Jan 2019** - 67kg
* **Feb 2019** - 65.5kg

## Resolution 5 🤝 ![](https://img.shields.io/badge/progress-20%25-red.svg)
This year my resolution is that I will publish at least 30 articles.

* [One Powerful Technique to Achieve More in 2019 📓](https://medium.com/@Sarthaksharma0/one-powerful-technique-to-achieve-more-in-2019-6f1d4b816d89)
* [Use the full power of Your Brain to be a Better Developer 🧠❤️👩🏻‍💻](https://dev.to/teamxenox/use-the-full-power-of-your-brain-to-be-a-better-developer--27pe)
* [15 underrated VSCode Themes for a change in 2019 🤷🏻‍♀️](https://dev.to/teamxenox/15-underrated-vscode-themes-for-a-change-in-2019---122e)
* [Do we really need a CSS Framework?](https://dev.to/sarthology/do-we-really-need-a-css-framework-4ma6)
* [What's the best source of caffeine for you?](https://dev.to/teamxenox/whats-the-best-source-of-caffeine-for-you--2lfd)
* [How to make Dynamic Text Overlays on Images?](https://dev.to/teamxenox/how-to-make-dynamic-text-overlays-on-images-dcc)

## Resolution 6 🤝 ![](https://img.shields.io/badge/progress-17%25-red.svg)
This year my resolution is that I will mentor 6 youngsters
- [x] [Pavan Jadhaw](https://github.com/pavanjadhaw)

Tweet me [@sarthology](https://twitter.com/sarthology)
